package glbm

import (
	"testing"
)

// TestServiceApi_ReLoad 测试 ServiceApi 的 ReLoad 方法及相关服务操作。
// 参数:
//
//	t - 测试框架的测试对象，用于报告测试结果。
//
// 返回值:
//
//	无返回值。
func TestServiceApi_ReLoad(t *testing.T) {
	// 创建一个名为"docker.service"，版本为"1"的服务实例。
	s := NewService("docker.service", "1")

	// 重新加载配置并重启服务，以验证服务能够正确处理配置更新和重启流程。
	s.ReLoad().ReStart()

	// 停止服务，以测试服务停止功能是否正常工作。
	s.Stop()

	// 启动服务，以验证服务启动功能是否正常工作。
	s.Start()

	// 重载全部守护进程配置并重启服务，用于测试在更新守护进程配置时的服务行为。
	s.ReLoadDaemon().ReStart()

	// 检查服务是否存在，如果存在，则报告错误。
	// 这里的逻辑似乎是为了测试服务在应该不存在的时候确实不存在，但实际逻辑可能需要进一步确认。
	if s.Exists() {
		t.Error("service exits")
	}

	// 再次启动服务，可能是为了测试服务在不同状态下的启动行为。
	s.Start()
}

// TestServiceApi_Exists 测试 ServiceApi 的 Exists 方法。
// 参数:
//
//	t - *testing.T，用于管理测试状态和报告测试失败信息。
//
// 返回值:
//
//	无返回值。
//
// 功能描述:
//
//	该函数通过创建服务实例并调用 Exists 方法，验证服务是否存在。
//	测试分为两个部分：
//	1. 验证一个存在的服务是否能正确返回 true。
//	2. 修改服务名称后，验证一个不存在的服务是否能正确返回 false。
func TestServiceApi_Exists(t *testing.T) {
	// 创建一个服务实例，参数1为服务名称，参数2为服务版本。
	s := NewService("docker.service", "1")

	// 检查服务是否存在，如果不存在则记录错误日志。
	if !s.Exists() {
		t.Errorf("service not exists")
	}
	t.Log("service exists")

	// 修改服务名称，模拟一个不存在的服务。
	s.Name = "dockers.service"

	// 再次检查服务是否存在，如果存在则记录错误日志。
	if s.Exists() {
		t.Errorf("service exists")
	}
	t.Log("service not exists")
}
