package glbm

import "gitee.com/liumou_site/gcs"

type DebPackageInfo struct {
	Package       string   // 包名
	Version       string   // 版本号
	Architecture  string   // 架构
	Maintainer    string   // 维护者
	InstalledSize int      // 安装大小（KB）
	Depends       []string // 依赖包
	Description   string   // 描述
	Homepage      string   // 主页
	Section       string   // 分类
	Priority      string   // 优先级
	Filename      string   // 文件名
	Size          int      // 文件大小（字节）
	MD5sum        string   // MD5校验和
}

// DpkgStruct 定义一个Dpkg管理结构
type DpkgStruct struct {
	password        string           // 主机密码
	Sudo            *gcs.ApiSudo     // 执行sudo命令
	Err             error            // 错误信息
	Result          bool             // 操作结果
	Debug           bool             // 是否开启Debug
	Info            bool             // 是否开启Info
	BlackHole       bool             // 是否使用黑洞模式(忽略错误信息)
	Ignore          bool             // 是否忽略标准输出
	PackageInfo     *DebPackageInfo  // 包信息
	PackageInfoList []DebPackageInfo // 包信息列表
}
