package glbm

import (
	"testing"
)

// TestDeveloper 是一个测试函数，用于检查 Developer 函数的返回值，
// 并根据其结果记录开发者模式的状态。
// 参数:
//
//	t *testing.T - 测试框架的核心结构体指针，用于管理测试状态和日志输出。
//
// 返回值:
//
//	无返回值。
func TestDeveloper(t *testing.T) {
	// 检查 Developer 函数的返回值以判断开发者模式是否开启。
	if Developer() {
		t.Logf("开发者模式已开启")
	} else {
		t.Log("开发者模式未开启")
	}
}

// TestCheckSudo 测试 CheckSudo 函数的行为。
// 参数:
//
//	t *testing.T - 测试框架的测试控制器，用于报告测试结果。
//
// 返回值:
//
//	无返回值。
//
// 功能:
//
//	该函数通过调用 CheckSudo 函数并传入不同的参数，验证其在不同情况下的行为是否符合预期。
//	主要测试两种场景：
//	1. 当传入 "1" 时，期望 CheckSudo 返回 true，表示成功获取 Sudo 权限。
//	2. 当传入 "2" 时，期望 CheckSudo 返回 false，表示未能获取 Sudo 权限。
func TestCheckSudo(t *testing.T) {
	// 检查 Sudo 权限获取是否成功
	if CheckSudo("1") {
		t.Log("Sudo权限获取成功")
	} else {
		t.Errorf("Sudo权限获取失败")
	}

	// 检查 Sudo 权限获取是否失败
	if CheckSudo("2") {
		t.Errorf("Sudo权限获取成功")
	} else {
		t.Logf("Sudo权限获取失败")
	}
}
