package glbm

import (
	"testing"
)

// TestApiFileSudo 测试文件操作的权限管理功能。
// 参数:
//
//	t *testing.T - 测试框架的测试控制器，用于记录测试结果和错误信息。
//
// 返回值:
//
//	无返回值。
func TestApiFileSudo(t *testing.T) {
	// 创建一个 FileSudo 实例，指定源文件路径和权限标识。
	fm := NewFileSudo("demo/s.txt", "1")

	// 调用 CopySudo 方法将源文件复制到目标路径。
	fm.CopySudo("demo/dst.txt")

	// 检查操作是否成功，并记录相应的测试日志。
	if fm.Err == nil {
		t.Log("复制成功")
	} else {
		t.Error("复制失败")
	}

	// 调用 DeleteSudo 方法删除源文件。
	fm.DeleteSudo("demo/s.txt")
}
