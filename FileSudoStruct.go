package glbm

import (
	"gitee.com/liumou_site/gcs"
)

// FileSudoStruct 文件管理(超级权限)
type FileSudoStruct struct {
	Password   string       // 密码
	sudo       *gcs.ApiSudo // Sudo实例
	FileStruct              // 继承文件结构
}
