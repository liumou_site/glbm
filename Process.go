package glbm

import (
	"gitee.com/liumou_site/gcs"
)

// ProcessIsRunning 检查指定的命令是否正在运行。
// 参数:
//
//	command (string): 需要检查的命令名称。
//
// 返回值:
//
//	bool: 如果指定的命令正在运行，则返回true；否则返回false。
func ProcessIsRunning(command string) bool {
	// 创建一个新的Shell实例。
	c := gcs.NewShell()

	// 执行命令以检查指定的命令是否正在运行。
	c.RunShell("ps -C ", command)

	// 如果命令执行成功且没有发生错误，
	// 并且命令的退出码为0，表示指定的命令正在运行。
	if c.Err == nil {
		if c.ExitCode == 0 {
			return true
		}
	}

	// 如果命令执行失败或指定的命令没有运行，则返回false。
	return false
}
