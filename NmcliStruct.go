package glbm

import (
	"gitee.com/liumou_site/gcs"
	"net"
)

// NmcliStruct 网卡管理
type NmcliStruct struct {
	conType        string      // 连接类型
	device         string      // 设备名称
	devList        []string    // 设备列表
	connectionList []string    // 连接列表
	deviceQuantity int         // 设备数量
	DefaultGw      net.IP      // 当前默认网关
	DefaultDevices string      // 当前默认网卡设备
	address        net.IP      // 当前IP
	mask           int         // 当前子网掩码
	mac            string      // 当前网卡Mac地址
	cmd            gcs.ApiSudo // 设置实例
	Err            error       // 错误信息
}

// NmcliConnectionStruct 网络连接管理结构
type NmcliConnectionStruct struct {
	Name       string        // 连接名称(例如: dhcp)
	Types      string        // 连接类型(bridge/wifi/ethernet)
	Method     string        // 连接模式(auto)
	uuid       string        // 连接UUID
	Dns        []string      // DNS列表
	Dev        string        // 设备名称
	Gw         net.IP        // 网关地址
	Address    net.IP        // IP地址
	Mask       int           // 子网掩码
	Err        error         // 错误信息
	cmd        *gcs.ApiShell // 命令实例
	UseConName string        // 正在使用的连接名称
	ConList    []string      // 连接列表
}
