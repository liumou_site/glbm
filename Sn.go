package glbm

import "gitee.com/liumou_site/gcs"

// GetOsSn 尝试以root权限获取操作系统序列号。
// 参数:
//
//	password - 用于提升权限的密码字符串。
//
// 返回值:
//
//	error - 如果执行过程中发生错误，可能不为nil。
//	string - 成功获取到的操作系统序列号字符串。
func GetOsSn(password string) (error, string) {
	// 创建一个新的Sudo请求对象，使用提供的密码。
	r := gcs.NewSudo(password)

	// 检查当前操作系统是否为Uos（统信操作系统）。
	if IsUos() {
		// 如果是Uos，执行命令来读取/etc/sninfo文件，其中可能包含系统序列号。
		r.RunShell("cat", "/etc/sninfo")
	} else {
		// 如果不是Uos，调用另一个函数尝试获取系统序列号。
		return GetOsSnSudo(password)
	}

	// 返回执行命令过程中可能遇到的错误，以及命令输出的字符串结果。
	return r.Err, r.Strings
}

// GetOsSnSudo 使用管理员权限执行命令以获取系统的序列号。
// 该函数需要用户输入密码以进行身份验证。
// 参数:
//
//	password - 用户的管理员密码，用于身份验证。
//
// 返回值:
//
//	error - 如果执行命令时发生错误，将返回该错误。
//	string - 成功执行命令后，返回系统的序列号。
func GetOsSnSudo(password string) (error, string) {
	// 创建一个具有管理员权限的命令运行器。
	r := gcs.NewSudo(password)

	// 使用管理员权限运行 "dmidecode" 命令以获取系统序列号。
	r.RunSudo("dmidecode", "-s", "system-serial-number")

	// 返回命令执行后的错误（如果有）和输出的序列号。
	return r.Err, r.Strings
}
