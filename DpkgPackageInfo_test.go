package glbm

import "testing"

// TestDpkgStruct_GetPackageInfo 测试 Dpkg 结构体的 GetPackageInfo 方法。
// 参数:
//
//	t - 测试框架的测试控制器，用于报告测试状态和错误信息。
//
// 返回值:
//
//	无返回值。
func TestDpkgStruct_GetPackageInfo(t *testing.T) {
	// 创建一个新的 Dpkg 实例，参数为 "1" 和 true。
	// 这里的 "1" 可能表示某种配置或标识，true 可能表示启用某些功能。
	dpkg := NewDpkg("1", true)

	// 调用 GetPackageInfo 方法，传入 .deb 文件路径以解析包信息。
	err := dpkg.GetPackageInfo("/home/xc/01.deb")
	if err != nil {
		// 如果方法返回错误，记录错误信息并标记测试失败。
		t.Errorf(err.Error())
	}

	// 输出获取到的包信息，用于验证解析结果是否正确。
	t.Log(dpkg.PackageInfo)
}
