package glbm

import (
	"testing"
)

// TestConnect 测试连接功能的函数。
// 参数:
//
//	t *testing.T - 测试框架的测试控制器，用于报告测试状态和错误。
//
// 返回值:
//
//	无返回值。
func TestConnect(t *testing.T) {
	// 创建一个新的连接对象，并启用某种配置（true 表示启用）。
	dpkg := NewConnect(true)
	logs.Info("开始新建")

	// 尝试添加连接配置，如果发生错误则记录错误信息并终止测试。
	err := dpkg.AddConnect()
	if err != nil {
		t.Errorf("配置错误")
		return
	}

	// 如果配置成功，记录日志表示配置完成。
	t.Log("配置完成")
}

// TestConnectDns 测试 DNS 配置的连接功能。
// 参数:
//
//	t - *testing.T 类型，用于管理测试过程中的日志记录和错误报告。
//
// 返回值:
//
//	无返回值。
func TestConnectDns(t *testing.T) {
	// 创建一个新的 Connect 实例，并启用 DNS 配置功能。
	dpkg := NewConnect(true)

	// 记录日志，表示开始添加 DNS 配置。
	logs.Info("开始添加")

	// 调用 AddDns 方法添加 DNS 配置，并检查是否发生错误。
	err := dpkg.AddDns()
	if err != nil {
		// 如果发生错误，记录错误信息并标记测试失败。
		t.Errorf("配置错误")
	}

	// 记录日志，表示 DNS 配置已完成。
	t.Log("配置完成")
}

// TestApiConnection_GetUseCon 测试 ApiConnection 的 GetUseCon 方法是否能够正确获取当前连接配置。
// 参数:
//
//	t - *testing.T 类型，用于管理测试过程中的错误报告和日志记录。
//
// 返回值:
//
//	无返回值。
func TestApiConnection_GetUseCon(t *testing.T) {
	// 创建一个新的连接对象，启用连接标志为 true。
	co := NewConnect(true)

	// 调用 GetUseCon 方法获取当前连接配置。
	co.GetUseCon()

	// 检查调用过程中是否发生错误，如果存在错误则记录失败日志。
	if co.Err != nil {
		t.Errorf("获取当前连接配置失败: %v", co.Err)
	}

	// 如果没有错误，记录成功日志。
	t.Log("获取当前连接配置成功")
}

// TestApiConnection_GetConList 测试 ApiConnection 的 GetConList 方法。
// 参数:
//
//	t - 测试框架的测试对象，用于报告测试结果。
//
// 返回值:
//
//	无返回值。
//
// 功能描述:
//
//	该函数通过创建一个新的连接对象并调用其 GetConList 方法，
//	验证获取当前连接配置列表的功能是否正常。
//	如果方法执行过程中发生错误，则记录错误信息；
//	如果成功，则打印连接列表以供验证。
func TestApiConnection_GetConList(t *testing.T) {
	// 创建一个新的连接对象，启用调试模式。
	co := NewConnect(true)

	// 调用 GetConList 方法获取当前连接配置列表。
	co.GetConList()

	// 检查连接对象的 Err 属性，判断是否发生错误。
	if co.Err != nil {
		// 如果存在错误，记录错误信息并标记测试失败。
		t.Errorf("获取当前连接配置失败: %v", co.Err)
	} else {
		// 如果没有错误，打印连接列表以供验证。
		t.Log("连接列表如下")
		t.Log(co.ConList)
	}
}
