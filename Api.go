package glbm

import (
	"net"
)

// EthInfo 网卡信息结构
type EthInfo struct {
	IP   net.IP   // IP地址
	GW   net.IP   // 网关地址
	MASK int      // 子网掩码
	DNS  []net.IP // DNS列表
}

// ApiEth 网卡信息结构体
type ApiEth struct {
	Index int    // 网卡设备索引
	Name  string // 网卡名称
	Ipv4  net.IP // IPV4地址
	Mask  int    //子网掩码
	Mac   string // Mac物理地址
	Err   error  // 错误信息
}

// OsInfo 系统信息结构
type OsInfo struct {
	Name     string // 系统名称
	Version  string // 系统版本
	CodeName string // 系统代号
	Arch     string // 系统架构
	IsMips   bool   // 是否属于Mips架构
	IsArm64  bool   // 是否属于ARM架构
	IsAmd64  bool   // 是否属于X86架构
}

// UserInfo 用户信息结构
type UserInfo struct {
	Name string // 用户名称
	Home string // 用户主目录
	UID  string // 用户UID
	Arch string // 系统架构
}
