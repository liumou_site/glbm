package glbm

import (
	"fmt"
	"strings"

	"gitee.com/liumou_site/gf"
)

// Start 启动指定的服务。
// 参数: 无
// 返回值:
//
//	*ServiceStruct - 返回当前 ServiceStruct 实例，以便支持链式调用。
//
// 注意:
//
//	该方法通过 systemctl 命令启动服务，并将执行过程中可能遇到的错误存储到 Err 属性中。
func (api *ServiceStruct) Start() *ServiceStruct {
	// 使用 systemctl 命令启动服务，命令前缀为 "systemctl start "
	api.sudo.RunSudo("systemctl start ", api.Name)

	// 将 SudoService 执行过程中可能遇到的错误赋值给 ServiceStruct 的 Err 属性
	api.Err = api.sudo.Err

	return api
}

// ReStart 重启当前服务实例所表示的服务。
// 该方法通过 sudo 特权执行 `systemctl restart` 命令来重启服务，
// 并将可能产生的错误存储到当前实例的 Err 字段中。
//
// 参数:
// 无
//
// 返回值:
// *ServiceStruct: 返回当前服务实例本身，以便支持链式调用。
func (api *ServiceStruct) ReStart() *ServiceStruct {
	// 使用 sudo 特权执行 systemctl restart 命令重启服务
	api.sudo.RunSudo("systemctl restart", api.Name)

	// 将 sudo 操作可能产生的错误赋值给当前实例的 Err 字段
	api.Err = api.sudo.Err

	return api
}

// Stop stops the service associated with the current ServiceStruct instance.
// It uses the systemctl command to stop the service and captures any potential error.
//
// Parameters:
//   - api: A pointer to the ServiceStruct instance. It contains the service name (api.Name)
//     and a sudo executor (api.sudo) used to run privileged commands.
//
// Returns:
//   - *ServiceStruct: The same instance (api) is returned to allow method chaining.
//     After execution, the Err field of the instance will contain any error
//     that occurred during the stop operation.
func (api *ServiceStruct) Stop() *ServiceStruct {
	// Executes the "systemctl stop" command for the service using sudo privileges.
	api.sudo.RunSudo("systemctl stop", api.Name)

	// Captures any error that occurred during the execution of the sudo command.
	api.Err = api.sudo.Err

	return api
}

// ReLoad 重新加载服务配置，通过sudo权限执行systemctl reload命令。
// 参数: 无
// 返回值:
//
//	*ServiceStruct - 返回当前ServiceStruct的指针，支持链式调用其他方法。
func (api *ServiceStruct) ReLoad() *ServiceStruct {
	// 使用sudo权限运行systemctl reload命令来重新加载服务配置。
	api.sudo.RunSudo("systemctl reload", api.Name)

	// 将sudo操作可能产生的错误赋值给api.Err，以便后续处理。
	api.Err = api.sudo.Err

	// 返回当前ServiceStruct的指针，允许链式调用其他方法。
	return api
}

// Enable 设置服务开机启动。
// 该方法通过 sudo 权限执行 "systemctl enable" 命令来启用服务，
// 并将执行过程中可能产生的错误存储到 Err 字段中。
//
// 参数:
//   - 无
//
// 返回值:
//   - *ServiceStruct: 返回当前实例的指针，以支持链式调用。
func (api *ServiceStruct) Enable() *ServiceStruct {
	// 使用 sudo 权限执行 "systemctl enable" 命令以启用服务。
	api.sudo.RunSudo("systemctl enable", api.Name)

	// 将 sudo 操作可能产生的错误赋值给 Err 字段。
	api.Err = api.sudo.Err

	return api
}

// Disable 禁止服务开机启动。
//
// 返回值: *ServiceStruct 返回API服务的实例，以便进行链式调用。
func (api *ServiceStruct) Disable() *ServiceStruct {
	api.sudo.RunSudo("systemctl disable", api.Name)
	api.Err = api.sudo.Err
	return api
}

// ReLoadDaemon 重新加载systemd守护进程配置。
// 这个方法不接受任何参数，因为它仅负责触发systemctl reload-daemon操作。
//
// 返回值: *ServiceStruct 返回API服务的实例，以便进行链式调用。
func (api *ServiceStruct) ReLoadDaemon() *ServiceStruct {
	api.sudo.RunSudo("systemctl reload-daemon")
	api.Err = api.sudo.Err
	return api
}

// Exists 检查当前服务是否存在于系统的服务列表中。
// 参数:
//   - api: 指向 ServiceStruct 的指针，包含服务名称 (api.Name) 和 sudo 命令执行器 (api.sudo)。
//
// 返回值:
//   - bool: 如果服务存在于系统服务列表中，则返回 true；否则返回 false。
func (api *ServiceStruct) Exists() bool {
	// 以sudo权限运行命令，列出所有服务的名称
	api.sudo.RunScriptSudo("systemctl -all| awk '{print $1}'")

	// 创建一个文件读取对象，并将sudo命令的输出赋值给它
	r := gf.NewReadFile("demo")
	r.Text = api.sudo.Strings

	// 将命令输出按行分割成服务列表
	services := strings.Split(r.Text, "\n")

	// 打印服务列表以便调试
	fmt.Println(services)

	// 遍历服务列表，检查是否包含当前服务名称
	for _, s := range services {
		if strings.Contains(s, api.Name) {
			// 如果找到服务名称，返回true
			return true
		}
	}

	// 如果服务列表中没有找到服务名称，返回false
	return false
}

// StatusGet 获取服务的当前状态。
// 该函数通过调用 systemctl 命令检查服务的 ActiveState 属性，并返回其值。
// 如果服务不存在、命令执行失败或无法解析状态，则返回 "unknown"。
//
// 参数:
// 无
//
// 返回值:
// string - 服务的状态，可能的值为 "active"、"inactive" 或 "unknown"。
func (api *ServiceStruct) StatusGet() string {
	// 检查服务是否存在，如果不存在则直接返回 "unknown"
	if !api.Exists() {
		return "unknown"
	}

	// 构造 systemctl 命令以获取服务的 ActiveState 属性
	// 使用参数化方式传递命令参数，防止命令注入攻击
	command := fmt.Sprintf("systemctl show %s --property=ActiveState", api.Name)
	api.sudo.RunShell(command)

	// 检查命令执行是否发生错误，如果有错误则返回 "unknown"
	if api.sudo.Err != nil {
		return "unknown"
	}

	// 解析命令输出，提取 ActiveState 的值
	lines := strings.Split(api.sudo.Strings, "\n")
	for _, line := range lines {
		if strings.HasPrefix(line, "ActiveState=") {
			parts := strings.SplitN(line, "=", 2)
			// 确保分割结果包含键和值两部分
			if len(parts) == 2 {
				return parts[1]
			}
		}
	}

	// 如果未找到 ActiveState 属性，返回 "unknown"
	return "unknown"
}
