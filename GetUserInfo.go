package glbm

import (
	"log"
	"os/user"
)

// GetUserInfo 获取当前用户的信息。
//
// 返回值:
// - *user.User: 指向 user.User 的指针，包含当前用户的信息。
// - error: 如果获取用户信息时发生错误，则返回该错误。
func GetUserInfo() (*user.User, error) {
	// 调用 user.Current() 获取当前用户信息。
	currentUser, err := user.Current()

	// 如果发生错误，记录错误日志并返回错误。
	if err != nil {
		log.Fatalf(err.Error())
		return nil, err
	}

	// 返回当前用户信息。
	return currentUser, nil
}
