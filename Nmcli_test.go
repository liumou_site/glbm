package glbm

import (
	"testing"
)

// TestDevInfo 测试获取所有以太网设备信息的功能。
// 参数:
//
//	t - *testing.T 类型，用于管理测试过程中的日志记录和错误报告。
//
// 返回值:
//
//	无返回值。
func TestDevInfo(t *testing.T) {
	// 创建一个新的Nmcli实例，用于与网络管理工具交互。
	n := NewNmcli()

	// 尝试获取所有以太网设备的信息。
	info, err := n.GetAllEthInfo()

	// 根据获取信息的结果进行相应的处理。
	if err == nil {
		// 如果获取信息成功，记录成功日志并打印每个设备的详细信息。
		t.Log("获取成功")
		for _, i := range info {
			t.Log(i)
		}
	} else {
		// 如果获取信息失败，记录错误日志。
		t.Error(err)
	}
}

// TestDefaultGw 测试获取默认网关的功能。
// 参数:
//
//	t - *testing.T 类型，用于管理测试过程中的日志记录和错误报告。
//
// 返回值:
//
//	无返回值。
func TestDefaultGw(t *testing.T) {
	// 创建Nmcli实例，用于操作网络相关的功能。
	n := NewNmcli()

	// 调用GetDefaultRouteInfo方法获取默认路由信息。
	res := n.GetDefaultRouteInfo()

	// 检查返回结果是否为nil，判断是否成功获取默认网关信息。
	if res == nil {
		t.Log("获取成功")
	} else {
		t.Errorf("获取失败: %v", n.Err)
	}

	// 打印默认网卡名称和默认网关信息，用于调试和验证。
	t.Logf("网卡名称: %s", n.DefaultDevices)
	t.Log(n.DefaultGw)
}

// TestApiNmcli_GetEthGw 测试 Nmcli 类的 GetEthGw 方法，用于获取指定网卡的网关信息。
// 参数:
//
//	t *testing.T - 测试框架的测试控制器，用于记录测试结果和错误信息。
//
// 返回值:
//
//	无返回值。
func TestApiNmcli_GetEthGw(t *testing.T) {
	// 创建 Nmcli 实例，用于调用其方法。
	n := NewNmcli()

	// 调用 GetEthGw 方法，尝试获取默认网卡的网关信息。
	res, err := n.GetEthGw(n.DefaultDevices)

	// 检查方法调用是否成功，并根据结果记录日志或错误信息。
	if err == nil {
		// 如果没有错误，记录成功的日志以及获取到的网关信息。
		t.Logf("获取成功")
		t.Log(res)
	} else {
		// 如果发生错误，记录错误信息。
		t.Errorf("获取失败: %v", err)
	}
}

// TestApiNmcli_GetEthInfo 测试 Nmcli 类的 GetEthInfo 方法，验证其获取指定网卡信息的功能。
// 参数:
//
//	t *testing.T - 测试框架的测试控制器，用于记录测试结果和错误信息。
//
// 返回值:
//
//	无返回值。
func TestApiNmcli_GetEthInfo(t *testing.T) {
	// 创建 Nmcli 实例，用于调用其方法。
	n := NewNmcli()

	// 获取第一个指定网卡的信息，并根据返回结果判断是否成功。
	res, err := n.GetEthInfo("enp125s0f1")
	if err == nil {
		t.Logf("获取成功")
		t.Log(res)
	} else {
		t.Errorf("获取失败: %s", err.Error())
	}

	// 获取第二个指定网卡的信息，并根据返回结果判断是否成功。
	res, err = n.GetEthInfo("wlan0")
	if err == nil {
		t.Logf("获取成功")
		t.Log(res)
	} else {
		t.Errorf("获取失败: %s", err.Error())
	}
}
