package glbm

import (
	"fmt"
	"gitee.com/liumou_site/gcs"
	"net"
	"os/exec"
	"strings"

	"gitee.com/liumou_site/gbm"
	"gitee.com/liumou_site/logger"
	"github.com/spf13/cast"
)

// Init 初始化ApiNmcli实例，检查nmcli命令是否存在并获取网络信息
// 该方法首先验证系统路径中是否存在nmcli命令，如果不存在，则记录错误信息并设置实例的错误状态
// 接着，尝试获取系统中所有网络接口的信息，如果获取失败，同样记录错误信息并设置实例的错误状态
func (n *NmcliStruct) Init() {
	// 判断系统是否安装nmcli命令
	if !gcs.CheckCmd("nmcli") {
		logger.Error("Nmcli Command does not exist")
		n.Err = fmt.Errorf("Nmcli Command does not exist")
	}
	// 尝试获取所有的网络接口信息
	_, err := n.GetAllEthInfo()
	if err != nil {
		logger.Error("获取网络信息失败")
		n.Err = err
		return
	}
}

// GetConnectionList 获取连接列表
// 本函数通过执行shell命令来获取特定设备类型的网络连接列表
// 它首先检查是否有设备列表，如果没有，则直接返回false
// 然后构造并执行一个shell命令来过滤出所需的连接类型和设备
// 最后，处理命令的输出，将其分割成字符串切片，并移除其中的空元素
// 返回值表示命令执行是否成功
func (n *NmcliStruct) GetConnectionList() bool {
	// 检查设备列表是否为空，如果为空，则返回false
	if len(n.devList) == 0 {
		return false
	}
	// 构造shell命令来获取连接列表
	c := fmt.Sprintf("nmcli connection |  grep %s | grep %s| awk '{print $1,$2}'", n.conType, n.device)
	// 执行shell命令
	n.cmd.RunShell(c)
	// 处理命令的输出，将其分割成字符串切片
	cs := strings.Split(n.cmd.Strings, "\n")
	// 移除切片中的空元素
	cs = gbm.SliceRemoveNull(cs)
	// 将处理后的连接列表保存到实例变量中
	n.connectionList = cs
	// 返回命令执行结果
	return n.cmd.Result
}

// IsIPv4 检查给定的IP地址是否是IPv4格式。
// 该函数首先使用net.ParseIP验证IP地址是否有效，然后通过检查地址字符串中是否包含"."
// 来确定它是否为IPv4地址。这种方法简单有效地区分IPv4和IPv6地址。
//
// 参数:
//
//	ipAddr (string): 需要验证的IP地址字符串。
//
// 返回值:
//
//	bool: 如果给定的IP地址是有效的IPv4地址，则返回true；否则返回false。
func IsIPv4(ipAddr string) bool {
	// 使用net.ParseIP验证IP地址是否有效。
	ip := net.ParseIP(ipAddr)
	// 检查解析结果是否非空且地址字符串中包含"."，以确认是IPv4地址。
	return ip != nil && strings.Contains(ipAddr, ".")
}

// IsLoopBackV4 检查IPv4地址是否为回环地址
// 参数:
//
//	ipAddr: 待检查的IPv4地址字符串
//
// 返回值:
//
//	bool: 如果地址是回环地址，则返回true；否则返回false
//
// 说明:
//
//	此函数用于确定给定的IPv4地址是否属于回环地址范围
//	目前仅将"127.0.0.1"视为回环地址，这是一个简化的判断逻辑
func IsLoopBackV4(ipAddr string) bool {
	return ipAddr == "127.0.0.1"
}

// IsDockerDevice 检查网络接口是否为 Docker 虚拟网桥
//
// 参数
//
// eth 是网络接口的名称
//
// 返回值
//
// 表示网络接口是否为 Docker 虚拟网桥 ("docker0")
func IsDockerDevice(eth string) bool {
	return eth == "docker0"
}

// GetAllEthInfo 获取所有以太网卡的信息
//
// 返回值:
//
// []ApiEth - 包含所有以太网卡信息的切片,
//
// error - 错误信息（如果有的话）
func (n *NmcliStruct) GetAllEthInfo() ([]ApiEth, error) {
	// 初始化一个空的以太网卡信息切片
	var EthInfo []ApiEth

	// 获取系统中所有网络接口的信息列表
	interfaceList, err := net.Interfaces()
	if err != nil {
		// 如果获取接口信息失败，记录错误日志并返回空的以太网卡信息和错误
		logger.Error("获取网卡列表失败: ", err)
		return EthInfo, err
	}

	// 遍历接口列表，获取每个接口的详细信息
	for _, address := range interfaceList {
		// 通过接口名称获取接口的详细信息
		byName, err := net.InterfaceByName(address.Name)
		if err != nil {
			// 如果获取接口详细信息失败，记录错误并返回当前收集到的以太网卡信息和错误
			fmt.Println(err)
			return EthInfo, err
		}

		// 获取当前接口的所有地址信息
		addrList, err := byName.Addrs()
		if err != nil {
			// 如果获取地址信息失败，记录错误日志并返回当前收集到的以太网卡信息和错误
			logs.Error(err.Error())
			return EthInfo, err
		}

		// 遍历地址信息，提取IP地址和子网掩码
		for _, Addr := range addrList {
			// 分割地址信息，获取IP地址和子网掩码
			IpInfo := strings.SplitN(Addr.String(), "/", 2)
			mask := cast.ToInt(IpInfo[1]) // 子网掩码
			ip := IpInfo[0]               // IP地址

			// 将当前接口名称添加到设备列表中
			n.devList = append(n.devList, address.Name)
			// 记录当前接口的MAC地址
			n.mac = cast.ToString(address.HardwareAddr)

			// 检查IP地址是否为IPv4地址、非回环地址且不属于Docker网卡
			if IsIPv4(ip) && !IsLoopBackV4(ip) && !IsDockerDevice(address.Name) {
				// 如果符合条件，创建一个ApiEth对象并添加到以太网卡信息切片中
				eth := ApiEth{
					Name:  address.Name,
					Index: address.Index,
					Ipv4:  net.ParseIP(ip),
					Mask:  mask,
					Mac:   n.mac}
				EthInfo = append(EthInfo, eth)
			} else {
				// 如果不符合条件，记录调试日志并跳过当前地址
				logger.Debug("略过非IPV4地址: ", ip)
			}
		}
	}

	// 设置设备数量为收集到的以太网卡信息的数量
	n.deviceQuantity = len(EthInfo)
	// 返回收集到的所有以太网卡信息和nil错误
	return EthInfo, nil
}

// GetEthGw 获取指定以太网接口的网关地址
// 参数:
//
//	eth: 以太网接口的名称
//
// 返回值:
//
//	net.IP: 网关的IP地址
//	error: 错误信息，如果执行过程中遇到任何问题，则返回错误
func (n *NmcliStruct) GetEthGw(eth string) (net.IP, error) {
	// 执行nmcli命令以获取指定以太网接口的IPv4网关信息
	n.cmd.RunShell("nmcli -f IP4.GATEWAY dev show", eth)
	// 检查命令执行过程中是否有错误
	if n.cmd.Err != nil {
		return nil, n.cmd.Err
	}
	// 分割命令输出以提取网关地址信息
	f := strings.Split(n.cmd.Strings, ":")
	// 确保输出格式符合预期，以便正确解析网关地址
	if len(f) != 2 {
		return nil, fmt.Errorf("无法通过 : 截取网关信息: %s", n.cmd.Strings)
	}

	// 解析网关地址
	gw := net.ParseIP(strings.Fields(f[1])[0])
	// 如果网关地址解析成功，则返回该地址
	if gw != nil {
		return gw, nil
	}
	// 如果解析失败，返回错误信息
	return nil, fmt.Errorf("数据匹配失败")
}

// GetEthInfo 获取指定网卡的详细信息，包括IP、网关、子网掩码和DNS服务器列表。
//
// 参数
//
// eth: 网卡名称，用于指定需要获取信息的网卡。
//
// 返回值
//
// EthInfo: 包含网卡详细信息的结构体，包括IP、GW、MASK和DNS列表。
//
// error: 如果获取信息过程中发生错误，返回错误信息。
func (n *NmcliStruct) GetEthInfo(eth string) (EthInfo, error) {
	// 初始化DNS列表为空数组，用于存储后续获取的DNS服务器IP地址。
	var dnsList []net.IP
	// 初始化exists变量为false，用于标记是否找到了指定的网卡。
	exists := false

	// 调用GetAllEthInfo方法获取所有网卡的信息。
	info, err := n.GetAllEthInfo()
	if err != nil {
		// 如果获取所有网卡信息时发生错误，返回空的EthInfo结构体和错误信息。
		return EthInfo{}, err
	}

	// 遍历所有网卡信息，寻找匹配指定名称的网卡。
	for i, v := range info {
		if v.Name == eth {
			// 当找到匹配的网卡时，将exists标记设置为true。
			exists = true // 证明匹配到了网卡
			// 执行命令获取指定网卡的详细信息。
			n.cmd.RunShell("nmcli device show", eth)
			if n.cmd.Err != nil {
				// 如果命令执行失败，记录警告日志。
				logs.Warn("设备信息获取失败")
			}
			// 从命令输出中提取DNS服务器的IP地址。
			n.cmd.Grep("IP4.DNS").Column(0, " ")
			if n.cmd.Err == nil {
				ds := strings.Split(n.cmd.Strings, "\n")
				if len(ds) == 0 {
					// 如果未找到DNS信息，记录警告日志。
					logger.Warn("DNS第一次获取失败")
				} else {
					// 遍历DNS信息，解析并添加到dnsList中。
					for _, v := range ds {
						ipCut := strings.Split(v, ":") // 通过冒号截取数据
						if len(ipCut) == 2 {
							// 如果截取的数据量等于2则进行截取
							ip := ipCut[1]
							IpAddr := net.ParseIP(ip)
							if IpAddr != nil {
								// 添加DNS信息到列表中，并记录调试日志。
								logs.Debug("添加DNS信息: ", IpAddr)
								dnsList = append(dnsList, IpAddr)
							}
						}
					}
				}
			}
			// 记录匹配成功的网卡信息。
			logger.Info("第: [ %d ]个网卡匹配成功", i)
			// 获取指定网卡的网关信息。
			gw, ge := n.GetEthGw(eth)
			if dnsList == nil {
				// 如果未获取到DNS信息，从系统配置文件中提取DNS信息。
				n.cmd.RunShell("grep nameserver /etc/resolv.conf")
				if n.cmd.Err == nil {
					dns := strings.Split(n.cmd.Strings, "\n")
					if len(dns) >= 1 {
						for _, v := range dns {
							ips := strings.Split(v, " ")
							if len(ips) == 2 {
								ip := ips[1]
								IpAddr := net.ParseIP(ip)
								if IpAddr != nil {
									dnsList = append(dnsList, IpAddr)
								}
							}
						}
					}
				}
			}
			// 根据获取到的信息，返回EthInfo结构体和可能的错误信息。
			if ge == nil {
				if dnsList != nil {
					return EthInfo{IP: v.Ipv4, GW: gw, MASK: v.Mask, DNS: dnsList}, nil
				} else {
					return EthInfo{IP: v.Ipv4, GW: gw, MASK: v.Mask, DNS: nil}, nil
				}
			} else {
				return EthInfo{IP: v.Ipv4, GW: gw, MASK: v.Mask, DNS: nil}, ge
			}
		}
	}
	// 如果未找到指定的网卡，构造错误信息并返回。
	getErr := fmt.Errorf("网卡不存在,无法获取信息")
	if exists {
		getErr = fmt.Errorf("网卡存在，但是无法获取网关")
	}
	return EthInfo{}, getErr
}

// GetDefaultRouteInfo 获取默认路由信息，包括默认网关地址和默认设备
// 该方法通过执行"ip -4 route show default"命令来获取默认路由信息，并解析命令输出以提取默认网关地址和默认设备
// 如果成功获取到默认网关地址和默认设备，则更新ApiNmcli实例的DefaultGw和DefaultDevices字段
// 如果在解析过程中遇到错误或找不到默认网关地址，则返回相应的错误
func (n *NmcliStruct) GetDefaultRouteInfo() error {
	// 执行"ip -4 route show default"命令以获取默认路由信息
	cmd := exec.Command("ip", "-4", "route", "show", "default")
	output, err := cmd.Output()
	if err != nil {
		return err
	}

	// 将命令输出按行分割成字符串数组
	lines := strings.Split(string(output), "\n")
	for _, line := range lines {
		// 查找以"default via "开头的行，这通常包含默认网关信息
		if strings.HasPrefix(line, "default via ") {
			fields := strings.Fields(line)
			fmt.Println(fields)
			// 确保字段数量足够，以避免索引越界
			if len(fields) >= 3 {
				// 解析并设置默认网关地址
				ip := net.ParseIP(fields[2])
				if ip != nil {
					n.DefaultGw = ip
				}
				logs.Debug("获取IP")
				fmt.Println(n.DefaultGw)
				// 设置默认设备
				n.DefaultDevices = fields[4]
				logs.Debug("获取设备")
				fmt.Println(n.DefaultDevices)
				// 如果默认网关地址解析失败，则返回错误
				if n.DefaultGw == nil {
					return fmt.Errorf("网关数据获取异常: %s", fields[2])
				}
				// 成功获取信息，返回nil表示无错误
				return nil
			}
		}
	}
	// 如果没有找到默认网关地址，则返回错误
	return fmt.Errorf("找不到默认网关地址")
}
