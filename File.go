package glbm

import "gitee.com/liumou_site/gf"

// Copy 方法用于将文件从源路径复制到目标路径。
// 参数:
//
//	dst string - 目标路径，指定文件需要复制到的位置。
//
// 返回值:
//
//	*FileStruct - 返回当前实例的指针，支持链式调用。
//
// 功能描述:
//
//	该方法首先设置目标路径，然后配置文件管理器的源路径和目标路径，
//	执行复制操作，并捕获复制过程中可能产生的错误信息。
func (api *FileStruct) Copy(dst string) *FileStruct {
	// 设置目标路径
	api.Dst = dst

	// 配置文件管理器的源路径和目标路径
	api.fileMan.Src = api.Src
	api.fileMan.Dst = api.Dst

	// 执行文件复制操作
	api.fileMan.Copy()

	// 获取并存储复制操作中的错误信息
	api.Err = api.fileMan.Err

	// 返回当前实例指针以支持链式调用
	return api
}

// Move 方法用于将文件从源路径移动到目标路径。
// 参数:
//
//	dst string - 目标路径，表示文件需要移动到的位置。
//
// 返回值:
//
//	*FileStruct - 返回当前实例的指针，以支持链式调用。
//
// 功能描述:
//
//	该方法首先设置目标路径，然后配置文件管理器的源路径和目标路径，
//	调用文件管理器的 Move 方法执行实际的移动操作，并捕获可能的错误信息。
func (api *FileStruct) Move(dst string) *FileStruct {
	// 设置目标路径
	api.Dst = dst

	// 配置文件管理器的源路径和目标路径
	api.fileMan.Src = api.Src
	api.fileMan.Dst = api.Dst

	// 调用文件管理器的 Move 方法执行文件移动操作
	api.fileMan.Move()

	// 获取文件移动操作的错误信息
	api.Err = api.fileMan.Err

	// 返回当前实例的指针，以支持链式调用
	return api
}

// Delete 删除指定路径的文件，并返回当前 FileStruct 实例的指针以支持链式调用。
// 参数:
//
//	无显式参数，但依赖于结构体 FileStruct 的 Src 字段，表示要删除的文件路径。
//
// 返回值:
//
//	*FileStruct: 返回当前实例的指针，允许链式调用其他方法。
//
// 注意:
//
//	删除操作的错误信息会被存储在结构体的 Err 字段中，调用者可以通过检查该字段来判断操作是否成功。
func (api *FileStruct) Delete() *FileStruct {
	// 创建文件对象并执行删除操作
	fs := gf.NewFile(api.Src)
	fs.Delete()
	fs.Close()

	// 获取删除操作的错误信息
	api.Err = fs.Err

	// 返回当前实例的指针，以支持链式调用
	return api
}

// DeleteFile 删除指定路径的文件，并返回当前实例以支持链式调用。
//
// 参数:
//
// 无参数，但依赖于结构体 FileStruct 的 Src 字段，表示要删除的文件路径。
//
// 返回值:
//
// 返回当前 FileStruct 实例的指针 (*FileStruct)，以便支持链式调用。
//
// 注意:
//
// 如果删除操作失败，错误信息会存储在结构体的 Err 字段中。
func (api *FileStruct) DeleteFile() *FileStruct {
	// 创建文件对象并执行删除文件操作
	fs := gf.NewFile(api.Src)
	fs.DeleteFile()
	fs.Close()

	// 获取删除操作的错误信息
	api.Err = fs.Err

	// 返回当前实例的指针，以支持链式调用
	return api
}
