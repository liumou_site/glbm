package glbm

import (
	"testing"
)

// TestApiFile_Copy 测试 File 结构体的 Copy 方法。
// 参数:
//
//	t *testing.T - 测试框架的测试控制器，用于记录测试结果和错误信息。
//
// 返回值:
//
//	无返回值。
//
// 功能:
//
//	该函数通过初始化 File 结构体实例并调用其 Copy 方法，
//	验证文件复制功能是否正常工作，并根据操作结果记录日志或错误信息。
func TestApiFile_Copy(t *testing.T) {
	// 初始化 File 结构体实例，指定源文件路径为 "demo/s.txt"。
	fm := NewFile("demo/s.txt")

	// 调用 Copy 方法，尝试将源文件复制到目标路径 "demo/dst.txt"。
	fm.Copy("demo/dst.txt")

	// 检查 File 结构体实例的 Err 字段，判断复制操作是否成功。
	if fm.Err == nil {
		// 如果没有错误，记录复制成功的日志信息。
		t.Log("复制成功")
	} else {
		// 如果存在错误，记录复制失败的错误信息，并包含具体的错误详情。
		t.Errorf("复制失败: %v", fm.Err)
	}
}

// TestApiFile_Delete 测试 File 类型的 Delete 方法是否能够正确删除指定文件夹。
// 参数:
//
//	t *testing.T - 测试框架的测试控制器，用于记录测试结果和错误信息。
//
// 返回值:
//
//	无返回值。
func TestApiFile_Delete(t *testing.T) {
	// 创建一个指向 'demo/dir' 的 File 实例，用于后续的删除操作。
	fm := NewFile("demo/dir")

	// 调用实例的 Delete 方法，尝试删除指定的文件夹。
	fm.Delete()

	// 检查删除操作的结果，判断是否成功执行。
	if fm.Err == nil {
		// 如果删除成功，记录一条日志消息，表明操作成功。
		t.Log("文件夹删除成功")
	} else {
		// 如果删除失败，记录错误信息，并提供具体的错误详情。
		t.Errorf("文件夹删除失败: %v", fm.Err)
	}
}

// TestApiFile_DeleteFile 测试删除文件的功能。
// 参数:
//
//	t *testing.T - 测试框架的测试控制器，用于报告测试结果。
//
// 返回值:
//
//	无返回值。
func TestApiFile_DeleteFile(t *testing.T) {
	// 创建一个名为 "demo/df.txt" 的文件对象。
	fm := NewFile("demo/df.txt")

	// 调用 Delete 方法尝试删除该文件。
	fm.Delete()

	// 检查删除操作是否成功。
	if fm.Err == nil {
		// 如果没有错误，记录删除成功的日志。
		t.Log("文件夹删除成功")
	} else {
		// 如果有错误，记录错误信息并标记测试失败。
		t.Errorf("文件夹删除失败: %v", fm.Err)
	}
}
