package glbm

import "gitee.com/liumou_site/logger"

var logs *logger.LocalLogger // 日志打印

// init函数用于初始化logs变量。
// 它创建了一个新的Logger实例，设置了版本信息和模块名称。
func init() {
	// 创建一个新的Logger实例，级别设置为3。
	logs = logger.NewLogger(3)
	// 设置Logger实例的版本信息。
	logs.Version = Version()
	// 设置Logger实例的模块名称。
	logs.Modular = "glbm"
}
