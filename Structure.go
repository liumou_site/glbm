package glbm

import (
	"strings"

	"gitee.com/liumou_site/gcs"
	"gitee.com/liumou_site/gf"
)

// NewNmcli 创建并初始化一个新的 NmcliStruct 实例。
// 该函数不接受任何参数。
// 返回值是一个指向 NmcliStruct 结构的指针，该结构用于网络管理。
func NewNmcli() *NmcliStruct {
	// 创建一个新的 NmcliStruct 实例。
	net := new(NmcliStruct)
	// 调用实例的 Init 方法进行初始化。
	net.Init()
	// 返回初始化后的实例。
	return net
}

// NewDpkg 创建一个新的 DpkgStruct 实例，并初始化其字段。
//
// 参数:
//   - password: 用于初始化 Sudo 实例的密码。
//   - realtime: 是否开启实时刷新执行命令过程。
//
// 返回值:
//   - *DpkgStruct: 返回一个初始化后的 DpkgStruct 指针。
func NewDpkg(password string, realtime bool) *DpkgStruct {
	n := new(DpkgStruct)
	n.password = password
	n.Sudo = gcs.NewSudo(password)
	n.Sudo.Realtime = realtime
	n.Info = false
	n.Debug = false
	n.Ignore = true     // 忽略标准输出
	n.BlackHole = false // 显示错误输出

	// 初始化 PackageInfo 和 PackageInfoList 字段
	n.PackageInfo = new(DebPackageInfo)
	n.PackageInfoList = make([]DebPackageInfo, 0)

	return n
}

// NewApt 创建并初始化一个新的ApiApt实例。
// 参数:
//
//	password: 用于身份验证的密码。
//	debug: 是否启用调试模式。
//	realtime: 是否启用实时模式。
//
// 返回值:
//
//	*AptStruct: 初始化后的ApiApt实例指针。
func NewApt(password string, debug, realtime bool) *AptStruct {
	// 创建ApiApt实例并初始化基本属性
	n := new(AptStruct)
	n.password = password
	n.Sudo = gcs.NewSudo(password)
	n.Sudo.Realtime = realtime
	n.Debug = debug
	n.Info = false
	n.Ignore = true     // 忽略标准输出
	n.BlackHole = false // 显示错误输出
	n.dpkg = NewDpkg(password, false)
	return n
}

// NewConnect 创建并初始化一个新的 NmcliConnectionStruct 实例。
// 参数 debug 指示是否启用调试模式。如果启用，将打印调试信息。
// 返回值是初始化后的 NmcliConnectionStruct 实例指针，如果初始化失败则返回 nil。
func NewConnect(debug bool) *NmcliConnectionStruct {
	// 创建一个新的 NmcliConnectionStruct 实例
	n := new(NmcliConnectionStruct)

	// 创建一个新的网络管理客户端
	nmcli := NewNmcli()

	// 获取默认路由信息，如果失败则返回 nil
	err := nmcli.GetDefaultRouteInfo()
	if err != nil {
		return nil
	}

	// 获取所有以太网接口信息，如果失败则记录错误并返回 nil
	eth, err := nmcli.GetAllEthInfo()
	if err != nil {
		logs.Error("本机网络信息获取失败")
		return nil
	}

	// 遍历并记录每个以太网接口的名称
	for _, i := range eth {
		logs.Debug(i.Name)
	}

	// 设置 DNS 服务器地址
	n.Dns = strings.Split("119.29.29.29 180.76.76.76", " ")

	// 设置当前默认IP、掩码、网卡设备和网关
	n.Address = nmcli.address    // 设置当前默认IP
	n.Mask = nmcli.mask          // 设置当前默认掩码
	n.Dev = nmcli.DefaultDevices // 设置当前默认网卡设备
	n.Gw = nmcli.DefaultGw       // 设置当前默认网关

	// 如果启用调试模式，打印详细的网络配置信息
	if debug {
		logs.Debug("(n.Address)设置当前默认IP: %s", n.Address)
		logs.Debug("(n.Mask)设置当前默认掩码: %d", n.Mask)
		logs.Debug("(n.Dev)设置当前默认网卡: %s", n.Dev)
		logs.Debug("(n.Gw)设置当前默认网关: %s", n.Gw)
		logs.Debug("(n.Dns)设置当前默认DNS: %s", n.Dns)
		logs.Debug("如需修改以上值请自行通过实例变量赋值")
	}

	// 返回初始化后的 NmcliConnectionStruct 实例
	return n
}

// NewService 创建并初始化一个新的ApiService实例。
// 参数name指定服务的名称，password用于验证身份。
// 返回值是初始化后的ApiService实例指针。
// 该函数主要负责设置ApiService实例的初始状态，
// 包括配置sudo工具和设置基本属性，为后续的操作打下基础。
func NewService(name, password string) *ServiceStruct {
	// 创建ApiService实例
	n := new(ServiceStruct)

	// 初始化sudo工具，用于执行需要身份验证的操作
	sudo := gcs.NewSudo(password)

	// 设置服务名称
	n.Name = name

	// 设置sudo工具实例
	n.sudo = sudo

	// 禁用sudo的实时模式，以优化性能或适应特定需求
	n.sudo.Realtime = false

	// 设置服务密码，用于后续的身份验证
	n.Password = password

	// 返回初始化后的ApiService实例
	return n
}

// NewFile 创建并初始化一个新的ApiFile实例。
// 参数src是文件路径，用于指定ApiFile处理的文件位置。
// 返回值是一个指向ApiFile的指针。
// 该函数首先通过new关键字在堆上分配一个ApiFile类型的内存空间，并将其地址赋值给变量n。
// 然后，使用src参数初始化ApiFile实例的Src字段，确定了文件的源路径。
// 接着，通过调用gcs.NewShell()方法初始化n的shell字段，为后续可能的shell命令执行做准备。
// 最后，利用gf.NewFileMan()方法，使用src参数初始化fileMan字段，设置文件操作的源和目标路径都为src，表明文件操作将在同一路径下进行。
// 完成初始化后，将n作为ApiFile实例的指针返回。
func NewFile(src string) *FileStruct {
	n := new(FileStruct)
	n.Src = src
	n.shell = gcs.NewShell()
	n.fileMan = gf.NewFileMan(src, src)
	return n
}

// NewFileSudo 创建并初始化一个新的 FileSudoStruct 实例。
// 该函数接收两个参数：src 作为文件源路径，password 作为解密密钥。
// 返回值是初始化后的 FileSudoStruct 实例指针。
func NewFileSudo(src, password string) *FileSudoStruct {
	// 创建一个新的 FileSudoStruct 实例。
	n := new(FileSudoStruct)

	// 使用提供的密码创建一个新的 Sudo 实例。
	sudo := gcs.NewSudo(password)

	// 将创建的 Sudo 实例赋值给 FileSudoStruct 实例的 sudo 属性。
	n.sudo = sudo

	// 设置文件源路径。
	n.Src = src

	// 禁用实时加密/解密。
	n.sudo.Realtime = false

	// 设置解密密钥。
	n.Password = password

	// 创建一个新的 Shell 实例。
	n.shell = gcs.NewShell()

	// 创建一个新的 FileMan 实例，用于管理文件。
	n.fileMan = gf.NewFileMan(src, src)

	// 返回初始化后的 FileSudoStruct 实例指针。
	return n
}
