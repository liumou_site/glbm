package glbm

import "gitee.com/liumou_site/gcs"

// AptStruct 是一个用于管理APT相关操作的结构体。
// 它包含了执行sudo命令、处理错误信息、以及控制日志和输出行为的字段。
type AptStruct struct {
	password  string       // password 是用于sudo权限的密码，确保在需要时可以提升权限。
	Sudo      *gcs.ApiSudo // Sudo 是一个指向 gcs.ApiSudo 的指针，用于执行需要sudo权限的命令。
	Err       error        // Err 用于存储操作过程中发生的错误信息，便于后续处理和调试。
	dpkg      *DpkgStruct  // dpkg 是一个指向 DpkgStruct 的指针，用于管理与dpkg相关的操作。
	Debug     bool         // Debug 表示是否开启调试模式。如果为 true，则会输出详细的调试信息。
	Info      bool         // Info 表示是否开启信息模式。如果为 true，则会输出一般的信息日志。
	BlackHole bool         // BlackHole 表示是否启用黑洞模式。如果为 true，则会忽略所有错误信息。
	Ignore    bool         // Ignore 表示是否忽略标准输出。如果为 true，则不会显示标准输出内容。
}
