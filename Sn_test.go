package glbm

import (
	"testing"
)

// TestGetOsSn 测试 GetOsSn 函数，验证其能否正确获取操作系统序列号。
// 该函数没有输入参数。
// 该函数检查操作系统序列号是否成功获取，并在成功时打印出来。
// 这是一个典型的单元测试函数，用于确保 GetOsSn 函数的正确性。
func TestGetOsSn(t *testing.T) {
	er, sn := GetOsSn("")
	if er == nil {
		t.Error(er)
	}
	t.Logf("sn: %s", sn)
}
