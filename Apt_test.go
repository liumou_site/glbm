package glbm

import (
	"testing"
)

// TestApt 是一个测试函数，用于验证 Apt 类型的 Install 方法是否能够正确安装软件包。
// 参数:
//
//	t - *testing.T 类型，用于管理测试状态和报告测试失败信息。
//
// 返回值:
//
//	无返回值。
func TestApt(t *testing.T) {
	// 创建一个新的 Apt 实例，参数分别为 ID、是否启用、是否强制。
	apt := NewApt("1", true, false)

	// 尝试安装软件包 "vsftpd"，并检查安装结果。
	i := apt.Install("vsftpd")
	if i {
		// 如果安装成功，记录日志。
		t.Log("安装成功")
	} else {
		// 如果安装失败，输出错误信息。
		t.Errorf("Failed to install: %v", apt.Err)
	}
}

// TestAptUninstall 测试 Apt 实例的卸载功能。
// 参数:
//
//	t - 测试框架的测试控制器，用于报告测试结果。
//
// 返回值:
//
//	无返回值。
func TestAptUninstall(t *testing.T) {
	// 创建一个 Apt 实例，参数分别为实例ID、是否模拟、是否自动确认。
	apt := NewApt("1", true, true)

	// 尝试卸载名为 "vsftpd" 和 "ftp" 的软件包，并检查卸载结果。
	// 如果卸载成功，记录日志；如果失败，记录错误信息。
	r := apt.Uninstall("vsftpd", "ftp")
	if r {
		t.Log("Uninstall is succeeded")
	} else {
		t.Errorf("Uninstall is Failed: %v", apt.Err)
	}

	// 再次尝试卸载名为 "vsft" 和 "ftp" 的软件包，以测试不同的卸载场景。
	// 这里预期卸载失败，因此如果卸载成功，则记录为错误。
	r2 := apt.Uninstall("vsft", "ftp")
	if r2 {
		t.Error("Uninstall is succeeded")
	} else {
		t.Log("Uninstall is Failed")
	}
}
