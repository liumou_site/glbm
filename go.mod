module gitee.com/liumou_site/glbm

go 1.23.0

require (
	gitee.com/liumou_site/gbm v1.1.7
	gitee.com/liumou_site/gcs v1.8.3
	gitee.com/liumou_site/gf v1.3.5
	gitee.com/liumou_site/logger v1.2.1
	github.com/spf13/cast v1.7.1
)

require (
	github.com/gabriel-vasile/mimetype v1.4.8 // indirect
	github.com/mattn/go-colorable v0.1.14 // indirect
	github.com/mattn/go-isatty v0.0.20 // indirect
	golang.org/x/net v0.37.0 // indirect
	golang.org/x/sys v0.31.0 // indirect
)
