package glbm

import (
	"container/list"
	"fmt"
	"strings"

	"gitee.com/liumou_site/gcs"
	"gitee.com/liumou_site/gf"
)

// Developer 检测当前系统是否处于开发者模式。
// 该函数首先获取当前系统的类型，如果系统类型不是UOS，则直接返回true，
// 表示无需检测开发者模式。如果是UOS系统，则检查特定的文件是否存在，
// 以及文件中的内容是否为"1"，以确定系统是否处于开发者模式。
//
// 返回值：
//
// bool类型，如果系统处于开发者模式或不是UOS系统，则返回true；否则返回false。
func Developer() bool {
	// 获取当前系统信息
	info, err := GetOsInfo()
	if err != nil {
		// 如果无法获取系统信息，则记录错误日志并返回false
		logs.Error("无法获取当前系统类型")
		return false
	}
	// 检查当前系统是否为UOS系统
	if info.Name != "uos" {
		// 如果不是UOS系统，则记录信息日志并返回true
		logs.Info("当前系统非UOS系统,无需检测开发者模式")
		return true
	}
	// 初始化Shell命令执行器
	gc := gcs.NewShell()
	gc.Debug = true
	// 创建链表，存储可能包含开发者模式信息的文件路径
	fileList := list.New()
	fileList.PushBack("/var/lib/deepin/developer-install_modes/enabled")
	fileList.PushBack("/var/lib/deepin/developer-install_mode/enabled")
	fileList.PushBack("/var/lib/deepin/developer-mode/enabled")
	// 遍历文件列表，检查每个文件是否存在，并读取文件内容
	for i := fileList.Front(); i != nil; i = i.Next() {
		// 获取当前文件路径
		file := fmt.Sprintln(i.Value)
		file = strings.Fields(file)[0]
		// 创建文件对象
		f := gf.NewFile(file)
		// 检查文件是否存在
		f.IsFile()
		// 如果文件存在，则尝试读取文件内容
		if f.IsFiles {
			gc.RunShell("cat", file)
			// 如果读取文件内容成功，并且内容为"1"，则表示系统处于开发者模式
			if gc.Err == nil {
				if gc.Strings == "1" {
					return true
				}
			} else {
				// 如果读取文件内容失败，则记录错误日志
				logs.Error(gc.Err.Error())
			}
		}
	}
	// 如果所有检查都未通过，则表示系统不处于开发者模式
	return false
}

// CheckSudo 检查提供的密码是否可以成功执行sudo命令。
// 该函数通过尝试执行需要sudo权限的命令来验证密码的有效性。
// 参数:
//
//	password - 用户提供的sudo密码。
//
// 返回值:
//
//	bool - 如果密码有效且命令执行成功，则返回true，否则返回false。
func CheckSudo(password string) bool {
	// 创建一个Sudo实例，用于执行需要sudo权限的命令。
	c := gcs.NewSudo(password)

	// 创建一个File实例，用于检查文件是否存在。
	f := gf.NewFile("/cmd.testing")

	// 检查文件是否存在。
	f.Exists()

	// 如果文件存在，尝试使用sudo权限删除文件。
	if f.ExIst {
		c.RunSudo("rm -f /cmd.testing")
	} else {
		// 如果文件不存在，尝试使用sudo权限创建文件。
		cmd := "touch /cmd.testing"
		c.RunSudo(cmd)

		// 如果文件创建成功，尝试使用sudo权限删除文件。
		if c.Err == nil {
			c.RunSudo("rm -f /cmd.testing")
		}
	}

	// 如果所有命令执行均未报错，则返回true，表示sudo密码有效。
	return c.Err == nil
}
