package glbm

// CopySudo 将文件或目录从源路径复制到目标路径，并使用 sudo 权限执行复制操作。
// 参数:
//
//	dst string - 目标路径，表示文件或目录将被复制到的位置。
//
// 返回值:
//
//	*FileSudoStruct - 返回当前结构体实例，以便支持链式调用。
//
// 注意:
//
//	如果复制过程中发生错误，错误信息会被记录到日志中，并存储在 Err 字段中。
func (api *FileSudoStruct) CopySudo(dst string) *FileSudoStruct {
	// 设置目标路径到结构体的 Dst 字段。
	api.Dst = dst

	// 使用 sudo 权限执行复制命令，将源路径的内容递归强制复制到目标路径。
	api.sudo.RunSudo("cp", "-rf", api.Src, api.Dst)

	// 检查并捕获复制过程中可能发生的错误。
	api.Err = api.sudo.Err
	if api.Err != nil {
		// 如果发生错误，记录错误日志。
		logs.Error("文件复制失败: ", api.Err.Error(), "\n")
	}

	// 返回当前结构体实例以支持链式调用。
	return api
}

// MoveSudo 将文件从源路径移动到目标路径，并使用 sudo 权限执行操作。
// 参数:
//
//	dst (string): 目标文件路径，表示文件需要移动到的位置。
//
// 返回值:
//
//	*FileSudoStruct: 返回当前结构体实例，以便支持链式调用。
//
// 注意:
//
//	如果移动操作失败，错误信息会被记录到日志中，并存储在 api.Err 中。
func (api *FileSudoStruct) MoveSudo(dst string) *FileSudoStruct {
	// 设置目标路径到结构体的 Dst 字段。
	api.Dst = dst

	// 使用 sudo 权限执行文件移动命令。
	api.sudo.RunSudo("mv", api.Src, api.Dst)

	// 检查并记录移动操作中的错误信息。
	api.Err = api.sudo.Err
	if api.Err != nil {
		logs.Error("文件移动失败: ", api.Err.Error(), "\n")
	}

	// 返回当前结构体实例以支持链式调用。
	return api
}

// DeleteSudo 使用sudo权限删除指定的文件或目录。
// 参数:
//
//	filename - 需要删除的文件或目录的名称。
//
// 返回值:
//
//	返回FileSudoStruct类型的指针，用于链式调用。
func (api *FileSudoStruct) DeleteSudo(filename string) *FileSudoStruct {
	// 使用sudo权限运行"rm -rf"命令删除文件或目录。
	api.sudo.RunSudo("rm", "-rf", filename)
	// 将sudo操作的错误信息赋值给api的Err属性。
	api.Err = api.sudo.Err
	// 如果存在错误，记录错误日志。
	if api.Err != nil {
		logs.Error("文件删除失败: ", api.Err.Error(), "\n")
	}
	// 返回api的指针，以支持链式调用。
	return api
}

// DeleteFileSudo 删除指定文件，并通过 sudo 权限执行删除操作。
// 参数:
//
//	filename - 要删除的文件的路径和名称。
//
// 返回值:
//
//	返回当前 FileSudoStruct 实例，以便支持链式调用。
//
// 注意:
//
//	如果删除失败，错误信息会被记录到日志中，并存储在 api.Err 中。
func (api *FileSudoStruct) DeleteFileSudo(filename string) *FileSudoStruct {
	// 使用 sudo 权限运行删除命令 "rm -f"。
	api.sudo.RunSudo("rm", "-f", filename)

	// 检查并捕获删除操作中的错误。
	api.Err = api.sudo.Err
	if api.Err != nil {
		// 如果删除失败，记录错误日志。
		logs.Error("文件删除失败: ", api.Err.Error(), "\n")
	}

	// 返回当前实例以支持链式调用。
	return api
}
