package glbm

import (
	"fmt"
	"net"
	"strings"

	"gitee.com/liumou_site/gcs"
	"gitee.com/liumou_site/logger"
)

// AddConnect 方法用于通过 nmcli 工具添加一个新的网络连接。
// 该方法会设置连接的相关参数并执行 nmcli 命令来创建连接。
//
// 参数:
// 无
//
// 返回值:
// error: 如果命令执行失败，返回错误信息；否则返回 nil。
func (c *NmcliConnectionStruct) AddConnect() error {
	// 设置API连接的地址为本地回环地址
	c.Address = net.ParseIP("127.0.0.1")

	// 设置DNS服务器地址列表
	c.Dns = strings.Split("114.114.114.114 8.8.8.8", " ")

	// 设置网关地址
	c.Gw = net.ParseIP("10.1.1.1")

	// 设置子网掩码长度
	c.Mask = 24

	// 设置连接的唯一标识符
	c.uuid = "as216a5w4d1646"

	// 设置连接的方法为自动
	c.Method = "auto"

	// 设置连接类型为以太网
	c.Types = "ethernet"

	// 设置连接的名称
	c.Name = "test"

	// 设置连接对应的设备名
	c.Dev = "eth0"

	// 构造添加网络连接的命令
	cmd := fmt.Sprintf("nmcli connection add type %s  con-Name %s ifname %s", c.Types, c.Name, c.Dev)

	// 打印命令以便调试
	fmt.Println(cmd)

	// 创建并运行shell命令
	command := gcs.NewShell()
	command.RunShell(cmd)

	// 返回命令执行的错误信息
	return command.Err
}

// AddDns 为指定的网络连接配置DNS服务器地址。
// 参数:
//   - c: NmcliConnectionStruct 的指针，包含网络连接的 UUID 和 DNS 服务器列表。
//
// 返回值:
//   - error: 如果 DNS 列表为空或执行命令失败，则返回错误；否则返回 nil。
func (c *NmcliConnectionStruct) AddDns() (err error) {
	shell := gcs.NewShell()

	fmt.Println("增加DNS")

	// 检查 DNS 列表是否至少包含一个服务器地址。
	if len(c.Dns) >= 1 {
		for index, server := range c.Dns {
			// 对于 DNS 列表中的第一个和第二个服务器地址，使用不同的 nmcli 命令进行配置。
			// 第一个地址直接设置为主 DNS，后续地址追加为备用 DNS。
			if index == 1 {
				cmd := fmt.Sprintf("nmcli connection modify %s ipv4.Dns %s", c.uuid, server)
				shell.RunShell(cmd)
				return shell.Err
			} else {
				cmd := fmt.Sprintf("nmcli connection modify %s +ipv4.Dns %s", c.uuid, server)
				shell.RunShell(cmd)
				return shell.Err
			}
		}
	} else {
		// 如果 DNS 列表为空，记录错误日志并返回错误。
		logger.Error("当前实例未配置DNS服务器信息,请先把DNS信息赋值到结构体中")
		err = fmt.Errorf("dns服务器列表为空")
		return err
	}
	return nil
}

// GetUseCon 获取当前活动的网络连接信息。
// 该函数通过执行 nmcli 命令来获取活动连接，并返回一个字符串切片，其中每个元素表示命令输出的一行。
// 参数:
//
//	无
//
// 返回值:
//
//	[]string: 包含活动连接信息的字符串切片。如果命令执行失败或输出为空，则返回 nil。
//
// 注意:
//
//	如果命令执行失败或输出为空，函数会记录错误日志并设置 c.Err。
func (c *NmcliConnectionStruct) GetUseCon() []string {
	// 执行命令以获取当前活动的网络连接信息
	cmd := "nmcli connection show --active"
	c.cmd.RunShell(cmd)
	c.Err = c.cmd.Err

	// 检查命令执行是否出错，如果出错则记录错误日志并返回
	if c.cmd.Err != nil {
		logger.Error("无法获取当前连接配置,执行的命令: ", cmd)
		return nil
	}

	// 将命令输出按行分割为字符串切片，便于后续处理
	sp := strings.Split(c.cmd.Strings, "\n")

	// 检查命令输出是否为空，如果为空则记录错误日志并返回
	if len(sp) == 0 {
		logger.Error("命令执行成功,但无法获取内容,请检查nmcli服务")
		c.Err = fmt.Errorf("命令执行成功,但无法获取内容,请检查nmcli服务")
	}

	// 返回分割后的命令输出，用于调试或查看结果
	return sp
}

// GetConList 获取当前系统中所有网络连接的名称列表。
// 该函数通过调用 nmcli 命令获取连接信息，并将结果解析为连接名称列表。
// 参数:
//
//	无
//
// 返回值:
//
//	无（通过结构体字段 c.ConList 存储结果，c.Err 存储错误信息）
func (c *NmcliConnectionStruct) GetConList() {
	// 定义命令以获取所有网络连接
	cmd := "nmcli connection show"
	// 执行命令并存储结果
	c.cmd.RunShell(cmd)

	// 检查命令执行是否出错，记录错误日志并返回
	if c.cmd.Err != nil {
		c.Err = c.cmd.Err
		logger.Error("无法获取当前连接配置,执行的命令: ", cmd)
		return
	}

	// 将命令执行结果按行分割，检查结果是否为空
	sp := strings.Split(c.cmd.Strings, "\n")
	if len(sp) == 0 {
		logger.Error("命令执行成功,但无法获取内容,请检查nmcli服务")
		c.Err = fmt.Errorf("命令执行成功,但无法获取内容,请检查nmcli服务")
		return
	}

	// 提取命令输出中的第二列（连接名称）
	c.cmd.Column(1, "")

	// 检查提取过程中是否有错误，记录日志并返回
	if c.Err != nil {
		logs.Error("数据截取失败")
		return
	}

	// 再次分割处理后的命令输出为多行，检查结果是否为空
	cl := strings.Split(c.cmd.Strings, "\n")
	if len(cl) == 0 {
		c.Err = fmt.Errorf("无法获取连接列表")
		return
	}

	// 遍历分割后的结果，排除表头"NAME"，并将连接名称添加到连接列表中
	for _, v := range cl {
		if v != "NAME" {
			c.ConList = append(c.ConList, v)
		}
	}
}

// GetConDnsList 获取指定连接的DNS列表
// 该方法通过执行shell命令来获取指定连接的DNS信息
// 参数:
//
//	con: 指定的连接名称
//
// 返回值: 无
func (c *NmcliConnectionStruct) GetConDnsList(con string) {
	// 构建获取连接信息的命令
	cmd := "nmcli connection show " + con

	// 执行命令
	c.cmd.RunShell(cmd)

	// 检查命令执行是否有误
	if c.cmd.Err != nil {
		// 记录错误信息
		c.Err = c.cmd.Err
		// 日志记录: 无法获取当前连接配置,执行的命令: [cmd]
		logger.Error("无法获取当前连接配置,执行的命令: ", cmd)
		return
	}
}
