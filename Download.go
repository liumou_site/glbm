package glbm

import (
	"fmt"
	"gitee.com/liumou_site/gf"

	"gitee.com/liumou_site/gcs"
	"github.com/spf13/cast"
)

// WgetToSave 下载指定URL的文件并保存为指定的文件名。
// 参数:
//
//	url: 需要下载的文件的URL。
//	filename: 下载文件后保存的文件名。
//
// 返回值:
//
//	bool: 下载是否成功。
func WgetToSave(url, filename string) bool {
	// 检查系统中是否存在wget命令。
	if gcs.CheckCmd("wget") {
		// 创建一个新的Shell实例以执行wget命令。
		run := gcs.NewShell()
		// 构造并执行wget命令，使用指定的URL和文件名。
		run.RunShell("wget -c --connect-timeout=5 --read-timeout=10  -O ", filename, " ", url)
		// 如果执行wget命令没有发生错误，则记录下载成功的日志并返回true。
		if run.Err == nil {
			logs.Info(fmt.Sprintf("Download Succeeded: %s", filename))
			return true
		} else {
			// 如果执行wget命令发生错误，则记录下载失败的日志。
			logs.Error(fmt.Sprintf("Download Failed: %s", filename))
		}
	} else {
		// 如果系统中不存在wget命令，则记录错误日志。
		logs.Error("Wget Command does not exist")
	}
	// 默认返回false，表示下载失败。
	return false
}

// CurlToSave 下载文件并保存到指定路径。
// 参数:
//
//	url: 需要下载的文件URL。
//	filename: 文件保存的路径。
//	cover: 是否覆盖已存在的文件。
//
// 返回值:
//
//	bool: 下载并保存成功返回true，否则返回false。
func CurlToSave(url, filename string, cover bool) bool {
	// 检查curl命令是否存在。
	if gcs.CheckCmd("curl") {
		// 创建一个新的Shell实例来执行命令。
		run := gcs.NewShell()
		// 创建一个文件对象。
		f := gf.NewFile(filename)
		// 检查文件是否存在。
		f.IsFile()
		// 如果文件存在且允许覆盖，则删除现有文件。
		if f.IsFiles {
			if cover {
				f.DeleteFile() // 删除文件
			}
		}
		// 构建curl命令字符串。
		cmd := cast.ToString(fmt.Sprintf("curl -o %s %s", filename, url))
		// 记录调试日志。
		logs.Debug(cmd)
		// 执行命令。
		run.RunShell(cmd)
		// 检查命令执行是否有错误。
		if run.Err == nil {
			// 如果没有错误，记录成功日志并返回true。
			logs.Info("Download Succeeded", filename)
			return true
		} else {
			// 如果有错误，记录错误日志并返回false。
			logs.Error("Download Failed", filename)
		}
	} else {
		// 如果curl命令不存在，记录错误日志并返回false。
		logs.Error("Curl Command does not exist")
	}
	return false
}
