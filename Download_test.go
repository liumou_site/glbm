package glbm

import (
	"testing"
)

// TestWget 测试从指定URL下载文件并保存到本地的功能。
// 参数:
//
//	t - 测试框架的测试对象，用于管理测试流程和报告测试结果。
//
// 返回值:
//
//	无返回值。
//
// 功能描述:
//
//	该函数调用WgetToSave方法，尝试从给定的URL下载文件并保存为本地文件。
//	如果下载成功，记录日志表示成功；如果失败，记录错误日志。
func TestWget(t *testing.T) {
	// 调用WgetToSave函数，尝试从指定URL下载文件并保存为本地文件。
	// 下载成功时返回true，失败时返回false。
	if WgetToSave("http://down.liumou.site/upload/Summary.py", "s.py") {
		// 记录下载成功的日志信息。
		t.Logf("下载成功")
	} else {
		// 记录下载失败的错误日志信息。
		t.Error("下载失败")
	}
}

// TestCurl 测试 CurlToSave 函数的功能。
// 参数:
//
//	t *testing.T - 测试框架的测试控制器，用于记录测试结果和错误信息。
//
// 返回值:
//
//	无返回值。
//
// 功能描述:
//
//	该函数尝试从指定的 URL 下载文件并保存为本地文件 's.py'。
//	使用 true 作为第三个参数调用 CurlToSave，可能表示启用某些特定的配置或选项。
//	如果下载成功，记录成功的日志；如果失败，记录错误信息。
func TestCurl(t *testing.T) {
	// 调用 CurlToSave 函数，尝试下载文件并保存为 's.py'。
	// 第三个参数 true 可能启用某些特定的配置或选项。
	if CurlToSave("http://down.liumou.site/upload/Summary.py", "s.py", true) {
		// 如果下载成功，记录成功的日志。
		t.Logf("下载成功")
	} else {
		// 如果下载失败，记录错误信息。
		t.Errorf("下载失败")
	}
}
