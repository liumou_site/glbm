package glbm

import (
	"testing"
)

// TestDpkg is a test function that verifies the installation status of specific packages using the Dpkg struct.
// Parameters:
// - t: A testing.T object used for logging test results and reporting errors.
// This function does not return any value.
func TestDpkg(t *testing.T) {
	// Initialize a new Dpkg instance with ID "1" and a boolean flag set to false.
	dpkg := NewDpkg("1", false)

	// Check if the package "docker-ce" is installed. If installed, log a success message;
	// otherwise, report an error indicating that the package is not installed.
	if dpkg.Installed("docker-ce") {
		t.Log("docker.io IS Installed")
	} else {
		t.Error("docker.io not installed")
	}

	// Check if the package "openssh-server" is installed. If installed, log a success message;
	// otherwise, report an error indicating that the package is not installed.
	if dpkg.Installed("openssh-server") {
		t.Log("SSh IS Installed")
	} else {
		t.Error("SSh not installed")
	}
}

// TestCheckIed 测试 CheckPacKey 方法的功能。
// 参数:
//
//	t *testing.T - 测试框架的测试控制器，用于报告测试结果和错误信息。
//
// 返回值:
//
//	无返回值。
//
// 功能描述:
//
//	该函数通过调用 Dpkg 的 CheckPacKey 方法，分别测试两个不同的包名 ("wps" 和 "dmi") 的查询功能。
//	如果查询成功，记录成功的日志；如果失败，记录错误信息。
func TestCheckIed(t *testing.T) {
	// 初始化一个 Dpkg 实例，传入 ID "1" 和布尔值 false。
	dpkg := NewDpkg("1", false)

	// 调用 CheckPacKey 方法检查 "wps" 包的密钥。
	// 如果查询成功，记录成功日志；否则记录错误信息。
	res, data := dpkg.CheckPacKey("wps", "code")
	if res {
		t.Log("查询成功: ", data)
	} else {
		t.Errorf("查询失败: %v", dpkg.Err)
	}

	// 调用 CheckPacKey 方法检查 "dmi" 包的密钥。
	// 如果查询成功，记录成功日志；否则记录错误信息。
	res1, data1 := dpkg.CheckPacKey("dmi", "code")
	if res1 {
		t.Logf("查询成功: %s", data1)
	} else {
		t.Errorf("查询失败: %v", dpkg.Err)
	}
}

// TestUninstall 测试卸载功能是否正常工作。
// 参数:
//
//	t *testing.T - 测试框架的测试控制器，用于报告测试结果和日志记录。
//
// 返回值:
//
//	无返回值。
func TestUninstall(t *testing.T) {
	// 创建一个 Dpkg 实例，模拟卸载操作的环境。
	dpkg := NewDpkg("1", true)

	// 尝试卸载 vsftpd，并根据卸载结果记录日志或报告错误。
	dpkg.Uninstall("vsftpd")
	if dpkg.Result {
		t.Log("卸载成功")
	} else {
		t.Errorf("卸载失败: %v", dpkg.Err)
	}

	// 尝试卸载 ftp，并根据卸载结果记录日志或报告错误。
	dpkg.Uninstall("ftp")
	if dpkg.Result {
		t.Log("卸载成功")
	} else {
		t.Errorf("卸载失败: %v", dpkg.Err)
	}
}

// TestApiDpkg_GetPackageStatus 测试 Dpkg 类的 GetPackageStatus 方法。
// 参数:
//
//	t - *testing.T 类型，用于管理测试过程中的状态和结果报告。
//
// 返回值:
//
//	无返回值。
//
// 功能描述:
//
//	该函数创建一个 Dpkg 实例，并调用其 GetPackageStatus 方法获取指定包的状态信息。
//	当前测试中，查询的包名为 "docker.io"，并将结果打印到控制台。
func TestApiDpkg_GetPackageStatus(t *testing.T) {
	// 创建一个新的 Dpkg 实例，参数为 "1" 和 false。
	dpkg := NewDpkg("1", false)

	// 调用 GetPackageStatus 方法获取 "docker.io" 包的状态信息。
	info := dpkg.GetPackageStatus("docker.io")

	// 打印获取到的包状态信息。
	t.Log(info)
}

// TestApiDpkg_ConfigureAll 测试 Dpkg 的 ConfigureAll 方法是否能够正确配置。
// 参数:
//
//	t - *testing.T 类型，用于管理测试过程中的日志记录和错误报告。
//
// 返回值:
//
//	无返回值。
func TestApiDpkg_ConfigureAll(t *testing.T) {
	// 创建一个新的 Dpkg 实例，ID 为 "1"，并且不启用某些特性（false）。
	dpkg := NewDpkg("1", false)

	// 调用 ConfigureAll 方法对 Dpkg 实例进行配置。
	dpkg.ConfigureAll()

	// 检查配置过程中是否发生错误，并根据结果记录日志或报告错误。
	if dpkg.Err == nil {
		t.Log("配置成功")
	} else {
		t.Error("配置失败")
	}
}
