package glbm

import "gitee.com/liumou_site/gcs"

// ServiceStruct 服务管理结构
type ServiceStruct struct {
	Name     string       // 服务名称
	Err      error        // 错误
	Status   string       // 当前状态
	Password string       // Sudo权限密码
	sudo     *gcs.ApiSudo // Sudo实例
}
