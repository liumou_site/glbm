package glbm

import (
	"testing"
)

// TestJurisdiction 测试权限相关的功能。
// 参数:
//
//	t *testing.T - 测试框架的测试控制器，用于记录测试日志和错误信息。
//
// 返回值:
//
//	无返回值。
func TestJurisdiction(t *testing.T) {
	// 检查当前用户是否具有超级用户权限，并记录测试结果。
	g := CheckSudo("1")
	if g {
		t.Log("密码检验正确")
	} else {
		t.Error("密码错误或无权限")
	}

	// 检查开发者模式是否已开启，并记录测试结果。
	d := Developer()
	if d {
		t.Logf("已开启开发者")
	} else {
		t.Logf("未开启开发者")
	}
}

// TestGetUserInfo 是一个单元测试函数，用于测试 GetUserInfo 函数的行为。
// 参数:
//
//	t *testing.T - 测试框架的测试控制器，用于记录测试日志和错误信息。
//
// 返回值:
//
//	无返回值。
func TestGetUserInfo(t *testing.T) {
	// 调用 GetUserInfo 函数获取用户信息，并检查是否发生错误。
	get, err := GetUserInfo()
	if err == nil {
		// 如果没有错误，打印用户信息到测试日志中。
		t.Logf("用户名: %s", get.Username)
		t.Logf("用户ID: %s", get.Uid)
		t.Logf("用户目录: %s", get.HomeDir)
	} else {
		// 如果发生错误，将错误信息记录为测试错误。
		t.Error(err.Error())
	}
}

// TestVersion is a test function designed to verify the behavior of the Version function.
//
// Parameters:
//   - t: A testing.T object used for managing test state and reporting test results.
//     It provides methods for logging, failure reporting, and skipping tests.
//
// This function calls the Version function without any additional logic or assertions.
// Ensure that the Version function is implemented and behaves as expected before running this test.
func TestVersion(t *testing.T) {
	// Calls the Version function to execute its logic.
	// No assertions or additional test logic are included in this test case.
	Version()
}

// TestOsInfo 测试获取操作系统信息的功能。
// 参数:
//
//	t *testing.T - 测试框架的测试控制器，用于记录日志或报告错误。
//
// 返回值:
//
//	无返回值。
func TestOsInfo(t *testing.T) {
	// 调用 GetOsInfo 函数获取操作系统信息，并检查是否发生错误。
	info, err := GetOsInfo()
	if err == nil {
		// 如果没有错误，记录操作系统的详细信息，包括名称、架构、版本和代号。
		t.Logf("系统类型: %s", info.Name)
		t.Logf("系统架构: %s", info.Arch)
		t.Logf("系统版本: %s", info.Version)
		t.Logf("系统代号: %s", info.CodeName)
	} else {
		// 如果发生错误，记录错误信息以便调试。
		t.Error(err.Error())
	}
}
