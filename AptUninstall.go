package glbm

import (
	"container/list"
	"fmt"
)

// UninstallList 卸载给定的软件包列表，并返回卸载成功和失败的软件包列表。
// 参数:
//
//	pacList: 包含需要卸载的软件包名称的链表，每个元素为一个软件包名称。
//
// 返回值:
//
//	ok: 卸载成功的软件包列表。
//	failed: 卸载失败的软件包列表。
func (api *AptStruct) UninstallList(pacList list.List) (ok, failed list.List) {
	// 初始化卸载成功和失败的链表
	uninstallListOk := list.New()
	uninstallListFailed := list.New()

	// 遍历输入的软件包列表，逐一处理每个软件包
	for i := pacList.Front(); i != nil; i = i.Next() {
		// 获取当前软件包名称
		pac := "" + fmt.Sprint(i.Value)
		// 调用 Uninstall 方法尝试卸载当前软件包
		un := api.Uninstall(pac, pac)
		// 根据卸载结果将软件包添加到对应的链表中
		if un {
			uninstallListOk.PushBack(pac)
		} else {
			uninstallListFailed.PushBack(pac)
		}
	}

	// 返回卸载成功和失败的软件包列表
	return *uninstallListOk, *uninstallListFailed
}

// UninstallSlice 尝试卸载给定的软件包列表，并将卸载结果分为成功和失败的列表返回。
// 参数:
//
//	pacList []string - 需要卸载的软件包名称列表。
//
// 返回值:
//
//	ok list.List - 卸载成功的软件包列表。
//	failed list.List - 卸载失败的软件包列表。
func (api *AptStruct) UninstallSlice(pacList []string) (ok, failed list.List) {
	// 初始化卸载成功的软件包列表
	uninstallListOk := list.New()
	// 初始化卸载失败的软件包列表
	uninstallListFailed := list.New()

	// 遍历软件包列表，逐一尝试卸载每个软件包
	for i := range pacList {
		pac := pacList[i]
		// 调用 Uninstall 方法卸载当前软件包
		un := api.Uninstall(pac, pac)
		// 根据卸载结果，将软件包添加到对应的列表中
		if un {
			uninstallListOk.PushBack(pac)
		} else {
			uninstallListFailed.PushBack(pac)
		}
	}

	// 返回卸载成功和失败的软件包列表
	return *uninstallListOk, *uninstallListFailed
}

// Uninstall 卸载指定的软件包，并根据操作结果记录日志。
// 参数:
//   - Package: 要卸载的软件包名称。
//   - name: 用于日志记录的软件包标识名称。
//
// 返回值:
//   - res: 表示卸载操作是否成功。如果软件包未安装，返回 true；否则根据卸载结果返回对应状态。
func (api *AptStruct) Uninstall(Package string, name string) (res bool) {
	// 将当前对象的屏蔽设置继承到sudo操作中
	api.Sudo.Ignore = api.Ignore
	api.Sudo.BlackHole = api.BlackHole

	// 检查软件包是否已安装
	ied := api.dpkg.Installed(Package)
	if ied {
		// 如果软件包已安装，执行卸载命令
		api.Sudo.RunSudo("apt purge -y", Package)

		// 根据卸载结果记录日志
		if api.Sudo.Err == nil && api.Info {
			logs.Info("[ %s ] Uninstallation succeeded", name)
		} else {
			logs.Error("[ %s ] Uninstallation Failed: ", name, api.Sudo.Err.Error())
		}

		// 更新错误信息
		api.Err = api.Sudo.Err
		return
	} else {
		// 如果软件包未安装，记录错误日志并返回成功结果
		api.Err = fmt.Errorf("[ %s ] Package is not installed", name)
		logs.Error(api.Err.Error())
		res = true
	}
	return
}
