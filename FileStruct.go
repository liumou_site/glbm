package glbm

import (
	"gitee.com/liumou_site/gcs"
	"gitee.com/liumou_site/gf"
)

// FileStruct 文件管理
type FileStruct struct {
	PathAbs    string        // 操作对象绝对路径
	PathBase   string        // 操作对象基础文件名
	PathFormat string        // 操作对象文件格式
	Src        string        // 源文件
	SrcAbs     string        // 源文件绝对路径
	SrcBase    string        // 源文件基础文件名
	SrcFormat  string        // 源文件格式
	Dst        string        // 目标文件
	DstAbs     string        // 目标文件绝对路径
	DstBase    string        // 目标文件基础文件名
	DstFormat  string        // 目标文件格式
	shell      *gcs.ApiShell // 命令实例
	Err        error         // 错误
	fileMan    *gf.FileMan   // 文件管理
}
